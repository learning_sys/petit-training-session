import { useEffect, useState } from "react";
import { Link } from "react-router-dom";

const UseEffect = () => {
  const [count, setCount] = useState(0);

  const onClickAdd = () => {
    setCount(count + 1);
  };

  useEffect(() => console.log("コンポーネントマウント"), []);

  useEffect(() => console.log("count変更"), [count]);

  return (
    <>
      <p>count: {count}</p>
      <button onClick={onClickAdd}>加算</button>
      <br />
      <Link to={"/"}>戻る</Link>
    </>
  );
};

export default UseEffect;
