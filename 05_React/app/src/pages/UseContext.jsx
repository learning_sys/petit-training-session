import { useContext } from "react";
import { Link } from "react-router-dom";
import { ThemeContext } from "../App";

const UseContext = () => {
  const theme = useContext(ThemeContext);

  return (
    <>
      <button style={{ background: theme, color: "white" }}>Button</button>
      <br />
      <Link to={"/"}>戻る</Link>
    </>
  );
};

export default UseContext;
