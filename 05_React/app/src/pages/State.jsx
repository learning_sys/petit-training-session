import { Link } from "react-router-dom";
import Counter from "../components/Counter";

const State = () => {
  return (
    <>
      <Counter />
      <Link to={"/"}>戻る</Link>
    </>
  );
};

export default State;
