import { Link } from "react-router-dom";
import ParentComponent from "../components/ParentComponent";

const Props = () => {
  return (
    <>
      <ParentComponent />
      <Link to={"/"}>戻る</Link>
    </>
  );
};

export default Props;
