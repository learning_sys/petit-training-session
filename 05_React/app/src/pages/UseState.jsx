import { useState } from "react";
import { Link } from "react-router-dom";

const UseState = () => {
  const [count, setCount] = useState(0);

  const onClickAdd = () => {
    setCount(count + 1);
  };

  const onClickReset = () => {
    setCount(1);
  };

  return (
    <>
      <p>count: {count}</p>
      <button onClick={onClickAdd}>加算</button>
      <br />
      <button onClick={onClickReset}>リセット</button>
      <br />
      <Link to={"/"}>戻る</Link>
    </>
  );
};

export default UseState;
