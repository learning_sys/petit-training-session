import { useCallback, useState } from "react";
import { Link } from "react-router-dom";
import Button from "../components/Button";

const UseCallback = () => {
  const [count1st, setCount1st] = useState(0);
  const [count2nd, setCount2nd] = useState(0);

  const incrementFirstCounter = () => {
    setCount1st(count1st + 1);
  };

  const incrementSecondCounter = useCallback(() => {
    setCount2nd(count2nd + 10);
  }, [count2nd]);

  return (
    <>
      <Button handleClick={incrementFirstCounter} value={"+1"} />
      <br />
      <Button handleClick={incrementSecondCounter} value={"+10"} />
      <br />
      <Link to={"/"}>戻る</Link>
    </>
  );
};

export default UseCallback;
