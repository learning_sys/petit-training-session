import { Link } from "react-router-dom";

const Home = () => {
  return (
    <ol>
      <li>
        <Link to="/props">propsとは</Link>
      </li>
      <li>
        <Link to="/state">stateとは</Link>
      </li>
      <li>
        <span>React Hooksとは</span>
        <ol>
          <li>
            <Link to="/useState">useState</Link>
          </li>
          <li>
            <Link to="/useEffect">useEffect</Link>
          </li>
          <li>
            <Link to="/useContext">useContext</Link>
          </li>
          <li>
            <Link to="/useReducer">useReducer</Link>
          </li>
          <li>
            <Link to="/useCallback">useCallback</Link>
          </li>
          <li>
            <Link to="/useMemo">useMemo</Link>
          </li>
          <li>
            <Link to="/useRef">useRef</Link>
          </li>
        </ol>
      </li>
    </ol>
  );
};

export default Home;
