import { useState } from "react";
import { Link } from "react-router-dom";
import { useMemo } from "react";

const UseMemo = () => {
  const [count1st, setCount1st] = useState(1);
  const [count2nd, setCount2nd] = useState(2);

  const doubleFirstCounter = () => {
    console.log("count1st計算");
    return count1st * 2;
  };

  const doubleSecondCounter = useMemo(() => {
    console.log("count2nd計算");
    return count2nd * 2;
  }, [count2nd]);

  return (
    <>
      <label>{`count1st: ${doubleFirstCounter()}`}</label>
      <br />
      <label>{`count2nd: ${doubleSecondCounter}`}</label>
      <br />
      <button onClick={() => setCount1st(count1st + 1)}>1st</button>
      <br />
      <button onClick={() => setCount2nd(count2nd + 1)}>2nd</button>
      <br />
      <Link to={"/"}>戻る</Link>
    </>
  );
};

export default UseMemo;
