import { useRef, useState } from "react";
import { Link } from "react-router-dom";

const UseRef = () => {
  const inputEl = useRef(null);
  const [inputElement, setInputElement] = useState("");
  const [text, setText] = useState("");

  console.log("レンダリング");

  const handleClick = () => {
    setText(`useState: ${inputElement} / useRef: ${inputEl.current.value}`);
  };

  return (
    <>
      <input type="text" onChange={(e) => setInputElement(e.target.value)} />
      <br />
      <input ref={inputEl} type="text" />
      <br />
      <button onClick={handleClick}>set text</button>
      <br />
      <span>{text}</span>
      <br />
      <Link to={"/"}>戻る</Link>
    </>
  );
};

export default UseRef;
