import { createContext } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

import Home from "./pages/Home";
import Props from "./pages/Props";
import State from "./pages/State";
import UseState from "./pages/UseState";
import UseEffect from "./pages/UseEffect";
import UseContext from "./pages/UseContext";
import UseReducer from "./pages/UseReducer";
import UseCallback from "./pages/UseCallback";
import UseMemo from "./pages/UseMemo";
import UseRef from "./pages/UseRef";

export const ThemeContext = createContext();

const App = () => {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/props" element={<Props />} />
        <Route path="/state" element={<State />} />
        <Route path="/useState" element={<UseState />} />
        <Route path="/useEffect" element={<UseEffect />} />
        <Route
          path="/useContext"
          element={
            <ThemeContext.Provider value="blue">
              <UseContext />
            </ThemeContext.Provider>
          }
        />
        <Route path="/useReducer" element={<UseReducer />} />
        <Route path="/useCallback" element={<UseCallback />} />
        <Route path="/useMemo" element={<UseMemo />} />
        <Route path="/useRef" element={<UseRef />} />
      </Routes>
    </Router>
  );
};

export default App;
