const ChildrenComponent = ({ name, age, children }) => {
  return (
    <div>
      <p>Hello, {name}!</p>
      <p>Your age is {age}.</p>
      {children}
    </div>
  );
};

export default ChildrenComponent;
