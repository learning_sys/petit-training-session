import { useState } from "react";

const Counter = () => {
  const [countState, setCountState] = useState(0);

  let count = 0;

  const onClickButton = () => {
    setCountState(countState + 1);
    count++;
    console.log(`count: ${count}`);
  };

  return (
    <div>
      <p>state: {countState}</p>
      <p>変数: {count}</p>
      <button onClick={onClickButton}>Click me</button>
    </div>
  );
};

export default Counter;
