import { useEffect, useReducer } from "react";

const reducer = (state, action) => {
  switch (action.type) {
    case "CHANGE_NAME":
      return { ...state, name: action.payload };
    case "CHANGE_EMAIL":
      return { ...state, email: action.payload };
    default:
      return state;
  }
};

const Form = () => {
  const [state, dispatch] = useReducer(reducer, { name: "", email: "" });

  const handleChange = (event) => {
    dispatch({
      type: `CHANGE_${event.target.name.toUpperCase()}`,
      payload: event.target.value,
    });
  };

  useEffect(() => {
    console.log("state", state);
  }, [state]);

  return (
    <form>
      <input
        type="text"
        name="name"
        value={state.name}
        onChange={handleChange}
      />
      <br />
      <input
        type="email"
        name="email"
        value={state.email}
        onChange={handleChange}
      />
      <br />
      <button type="submit">Submit</button>
    </form>
  );
};

export default Form;
