import ChildrenComponent from "./ChildrenComponent";

const ParentComponent = () => {
  return (
    <ChildrenComponent name={"田中太郎"} age={29}>
      <p>Your sex is male</p>
    </ChildrenComponent>
  );
};

export default ParentComponent;
