import { memo } from "react";

const Button = memo(({ handleClick, value }) => {
  console.log("Button Value", value);

  return (
    <button type="button" onClick={handleClick}>
      {value}
    </button>
  );
});

export default Button;
