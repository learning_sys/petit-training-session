const doubleApi = (value) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(value * 2);
    }, 2000);
  });
};

const execute = () => {
  let result = 0;

  return doubleApi(5)
    .then((val) => {
      result += val;
      return doubleApi(10);
    })
    .then((val) => {
      result += val;
      return doubleApi(result);
    });
};

execute().then((v) => {
  console.log(v); // => 60
});
