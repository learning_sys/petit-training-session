const doubleApi = (value) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(value * 2);
    }, 2000);
  });
};

const execute = async () => {
  let result = 0;

  result += await doubleApi(5);
  result += await doubleApi(10);
  return await doubleApi(result);
};

execute().then((v) => {
  console.log(v); // => 60
});
