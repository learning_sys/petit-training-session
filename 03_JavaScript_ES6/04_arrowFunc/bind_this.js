const obj = {
  name: 'たけたい',
  hobby: '映画鑑賞',
  print: function () {
    console.log(this.name); // => たけたい
    const es5 = {
      do: function () {
        console.log(`従来: ${this.hobby}`); // => 従来: undefined
      },
    };
    const arrow = {
      do: () => console.log(`アロー関数: ${this.hobby}`), // => アロー関数: 映画鑑賞
    };
    es5.do();
    arrow.do();
  },
};
obj.print();
