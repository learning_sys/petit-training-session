const nums = [1, 2];
const numsJoin = [...nums, 3, 4];
console.log(...numsJoin); // => 1, 2, 3, 4
