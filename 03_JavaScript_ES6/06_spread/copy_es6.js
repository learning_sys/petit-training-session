const nums = [1, 2, 3, 4];
const numsCopy = [...nums];
console.log(...numsCopy); // => 1, 2, 3, 4
