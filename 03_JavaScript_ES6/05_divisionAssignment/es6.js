const person = {
  namae: 'たけたい',
  country: 'Japan',
};
const { namae, country } = person;

console.log(namae); // => たけたい
console.log(country); // => Japan
