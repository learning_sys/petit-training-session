const nums = [1, 2, 3, 4];
const numsAdd = nums.map((num) => num + 1);
console.log(...numsAdd); // => 2, 3, 4, 5
