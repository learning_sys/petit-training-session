const nums = [1, 2, 3, 4];
let existsEven = false;
for (let index = 0; index < nums.length; index++) {
  if (nums[index] % 2 === 0) {
    existsEven = true;
  }
}
console.log(existsEven); // => true
