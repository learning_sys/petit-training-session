const nums = [1, 2, 3, 4];
const numsEven = nums.filter((num) => num % 2 === 0);
console.log(...numsEven); // => 2, 4
