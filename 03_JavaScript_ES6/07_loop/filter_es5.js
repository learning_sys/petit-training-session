const nums = [1, 2, 3, 4];
const numsEven = [];
for (let index = 0; index < nums.length; index++) {
  if (nums[index] % 2 === 0) {
    numsEven.push(nums[index]);
  }
}
console.log(...numsEven); // => 2, 4
