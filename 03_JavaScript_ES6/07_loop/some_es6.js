const nums = [1, 2, 3, 4];
const existsEven = nums.some((num) => num % 2 === 0);
console.log(existsEven); // => true
