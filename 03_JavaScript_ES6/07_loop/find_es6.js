const nums = [1, 2, 3, 4];
const numsEvenFirst = nums.find((num) => num % 2 === 0);
console.log(numsEvenFirst); // => 2
