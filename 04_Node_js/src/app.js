const express = require("express");
const app = express();

// 追加
app.use(express.static("public"));

app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.get("/hello", (req, res) => {
  res.render("hello_world.ejs", {
    contents: "<p>hello, World</p>",
  });
});

app.listen(3000);
